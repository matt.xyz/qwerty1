import { Injectable } from '@angular/core';
import { DatosUsuario } from '../interfaces/tarjeta.interface';
@Injectable({
  providedIn: 'root'
})
export class TarjetaService {
   listUsuarios: DatosUsuario[] = [
    {titular:'Morgana Dark', numeroTarjeta: '193746593748', fechaExpiracion: '03/22'},
    {titular:'J-Hope', numeroTarjeta: '093749583628', fechaExpiracion: '12/29'},
    {titular:'Frank Spellman', numeroTarjeta: '153723542564', fechaExpiracion: '07/24'},
    {titular:'George Quie', numeroTarjeta: '102394876253', fechaExpiracion: '05/50'},
    {titular:'Jade Fox', numeroTarjeta: '172123456789', fechaExpiracion: '16/80'},
  ];
  constructor() { }

  getUsuario(){
    return this.listUsuarios.slice()
  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }
}
