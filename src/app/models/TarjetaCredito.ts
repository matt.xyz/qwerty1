//41.
export class TarjetaCredito {
  id?:string;
  titular:string
  numeroTarjeta:string;
  fechaExpiracion:string;
  clave:number;
  fechaCreacion:Date;
  fechaActualizacion:Date;

  constructor (titular:string,numeroTarjeta:string,fechaExpiracion:string,clave:number){
    this.titular=titular;
    this.numeroTarjeta=numeroTarjeta;
    this.fechaExpiracion=fechaExpiracion;
    this.clave=clave;
    this.fechaCreacion= new Date ();
    this.fechaActualizacion = new Date();
  }
}