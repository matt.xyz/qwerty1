export interface TarjetaCredito {

  titular:string
  numeroTarjeta:string;
  fechaExpiracion:string;
  clave:number;
  fechaCreacion:Date;
  fechaActualizacion:Date;
}